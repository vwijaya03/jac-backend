<?php

/*
 * This file is part of jwt-auth.
 *
 * (c) Sean Tymon <tymon148@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tymon\JWTAuth\Middleware;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Log;
use App\Http\Controllers\AESController;
use JWTAuth;

class GetUserFromToken extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $token = 'a';

        if($request->header('Authorization') != null)
        {
            $token = $request->header('Authorization');
        }

        if($request->get('token') != null)
        {
            $token = $request->get('token');
        }
        // if (! $token = $this->auth->setRequest($request)->getToken()) {
        //     return $this->respond('tymon.jwt.absent', 'token_not_provided', 400);
        // }

        try {
            $aes = new AESController();
            $decrypted_token = $aes->decrypt($token);
            
            $user = $this->auth->authenticate($decrypted_token);
        } catch (TokenExpiredException $e) {
            Log::info($e);
            return $this->respond('tymon.jwt.expired', 'token_expired', $e->getStatusCode(), [$e]);
        } catch (JWTException $e) {
            return response()->json(['error' => 'token_invalid'], 401);
            // return $this->respond('tymon.jwt.invalid', 'token_invalid', $e->getStatusCode(), [$e]);
        }

        if (! $user) {
            return $this->respond('tymon.jwt.user_not_found', 'user_not_found', 404);
        }

        $this->events->fire('tymon.jwt.valid', $user);

        return $next($request);
    }
}

// <?php

// /*
//  * This file is part of jwt-auth.
//  *
//  * (c) Sean Tymon <tymon148@gmail.com>
//  *
//  * For the full copyright and license information, please view the LICENSE
//  * file that was distributed with this source code.
//  */

// namespace Tymon\JWTAuth\Middleware;

// use Tymon\JWTAuth\Exceptions\JWTException;
// use Tymon\JWTAuth\Exceptions\TokenExpiredException;
// use Log;
// use App\Http\Controllers\AESController;
// use JWTAuth;

// class GetUserFromToken extends BaseMiddleware
// {
//     /**
//      * Handle an incoming request.
//      *
//      * @param  \Illuminate\Http\Request  $request
//      * @param  \Closure  $next
//      * @return mixed
//      */
//     public function handle($request, \Closure $next)
//     {
//         // $token = $request->header('Authorization');

//         if (! $token = $this->auth->setRequest($request)->getToken()) {
//             Log::info('masuk if pertama');
            
//             return $this->respond('tymon.jwt.absent', 'token_not_provided', 400);
//         }

//         try {
                // Log::info('masuk try catch');
                // Log::info('isi token nya: '.$token);
                // $aes = new AESController();
                // $decrypted_token = $aes->decrypt($token);

//             $user = $this->auth->authenticate($token);
//         } catch (TokenExpiredException $e) {
//             return $this->respond('tymon.jwt.expired', 'token_expired', $e->getStatusCode(), [$e]);
//         } catch (JWTException $e) {
//             return $this->respond('tymon.jwt.invalid', 'token_invalid', $e->getStatusCode(), [$e]);
//         }

//         if (! $user) {
//             return $this->respond('tymon.jwt.user_not_found', 'user_not_found', 404);
//         }

//         $this->events->fire('tymon.jwt.valid', $user);

//         return $next($request);
//     }
// }
