<?php

use Illuminate\Database\Seeder;
use App\ProductModel;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i=1; $i <= 100; $i++) {
			ProductModel::create
			([
				'category_id' => $faker->numberBetween($min = 1, $max = 30), 
				'title' => "Produk ".$i." ".$faker->name,
				'description' => $faker->text(),
				'img_path1' => '',
				'img_path2' => '',
				'img_path3' => '',
				'img_path4' => '',
				'delete' => 0
			]);
		}
    }
}
