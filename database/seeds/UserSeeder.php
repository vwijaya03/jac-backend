<?php

use Illuminate\Database\Seeder;
use App\UserModel;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserModel::create
        (
            [
                'fullname' => "Admin",
                'email' => "admin@yahoo.co.id",
                'username' => "admin",
                'password' => Hash::make('admin'),
                'telepon' => '081081081081',
                'kota' => 'Kota',
                'kecamatan' => 'Kota',
                'kode_pos' => '51231',
                'tipe' => 'admin',
                'status' => 1,
                'delete' => 0
            ]
        );

        UserModel::create
        (
            [
                'fullname' => "toko budi",
                'email' => "toko_budi@yahoo.co.id",
                'username' => "budi",
                'password' => Hash::make('budi'),
                'telepon' => '081081081081',
                'kota' => 'Solo',
                'kecamatan' => 'Kota',
                'kode_pos' => '51231',
                'tipe' => 'toko',
                'status' => 1,
                'delete' => 0
            ]
        );

        UserModel::create
        (
            [
                'fullname' => "toko andi",
                'email' => "toko_andi@yahoo.co.id",
                'username' => "andi",
                'password' => Hash::make('andi'),
                'telepon' => '081081081081',
                'kota' => 'Solo',
                'kecamatan' => 'Kota',
                'kode_pos' => '51231',
                'tipe' => 'toko',
                'status' => 1,
                'delete' => 0
            ]
        );
    }
}
