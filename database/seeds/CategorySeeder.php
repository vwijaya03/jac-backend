<?php

use Illuminate\Database\Seeder;
use App\CategoryModel;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create('fr_FR');
        for ($i=1; $i <= 30; $i++) {
			CategoryModel::create
			([
				'name' => "Kategori ".$i." ".$faker->name,
				'delete' => 0
			]);
		}
    }
}

// 'jumlah' => $faker->numberBetween($min = 100000, $max = 200000),