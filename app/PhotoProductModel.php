<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoProductModel extends Model
{
    protected $table = 'photo_products';
    protected $primaryKey = 'id';
    protected $fillable = ['product_id', 'img_path', 'delete'];
}
