<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturModel extends Model
{
    protected $table = 'retur';
    protected $primaryKey = 'id';
    protected $fillable = ['description', 'delete'];
}
