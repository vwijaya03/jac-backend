<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserModel extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $fillable = ['fullname', 'email', 'username', 'password', 'telepon', 'kota', 'kecamatan', 'kode_pos', 'tipe', 'status', 'delete'];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
