<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddReturRequest;
use App\Http\Requests\EditReturRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\ReturModel;
use Auth, Log, JWTAuth;

class ApiReturController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function getRetur()
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$payment = ReturModel::select('id', 'description')
    	->where('delete', 0)
    	->orderBy('created_at', 'desc')
    	->first();

    	if($payment == null)
    	{
    		return response()->json(['result' => []]);
    	}

    	return response()->json(['result' => $payment]);
    }

    public function postAddRetur(AddReturRequest $request)
    {	
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$payment = ReturModel::select('id', 'description')
    	->where('delete', 0)
    	->get();

    	if(count($payment) > 0)
    	{
    		return response()->json(['error' => 'Data informasi sudah ada.']);
    	}

    	ReturModel::create([
    		'description' => $request->get('description'),
    		'delete' => 0,
    	]);

        return response()->json(['success' => 'Data informasi berhasil di tambahkan.']);
    }

    public function getEditRetur($id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$payment = ReturModel::select('id', 'description')->where('id', $id)->where('delete', 0)->first();

    	if($payment == null)
    	{
    		return response()->json(['error' => 'Data tidak tersedia.']);
    	}

    	return response()->json(['result' => $payment]);
    }

    public function postEditRetur(EditReturRequest $request, $id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$payment = ReturModel::select('id')
    	->where('id', $id)
    	->where('delete', 0)
    	->first();

    	if($payment == null)
    	{
    		return response()->json(['error' => 'Data tidak tersedia.']);
    	}

    	ReturModel::where('id', $id)->where('delete', 0)
    	->update([
    		'description' => $request->get('description'),
    	]);

    	return response()->json(['success' => 'Data informasi berhasil di update.']);
    }

    public function postDeleteRetur($id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	ReturModel::where('id', $id)->where('delete', 0)
    	->update([
    		'delete' => 1
    	]);

    	return response()->json(['success' => 'Data informasi berhasil di hapus.']);
    }
}
