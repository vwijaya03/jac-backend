<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;

class AESController extends Controller
{
	// const M_CBC = 'cbc';
	// const M_CFB = 'cfb';
	// const M_ECB = 'ecb';
	// const M_NOFB = 'nofb';
	// const M_OFB = 'ofb';
	// const M_STREAM = 'stream';

	// protected $key;
	// protected $cipher;
	// protected $data;
	// protected $mode;
	// protected $IV;
	// /**
	// * 
	// * @param type $data
	// * @param type $key
	// * @param type $blockSize
	// * @param type $mode
	// */
	// function __construct($data = null, $key = null, $blockSize = null, $mode = null) {
	// 	$this->setData($data);
	// 	$this->setKey($key);
	// 	$this->setBlockSize($blockSize);
	// 	$this->setMode($mode);
	// 	$this->setIV("");
	// }

	// /**
	// * 
	// * @param type $data
	// */
	// public function setData($data) {
	// 	$this->data = $data;
	// }

	// /**
	// * 
	// * @param type $key
	// */
	// public function setKey($key) {
	// 	$this->key = $key;
	// }

	// /**
	// * 
	// * @param type $blockSize
	// */
	// public function setBlockSize($blockSize) {
	// 	switch ($blockSize) {
	// 		case 128:
	// 			$this->cipher = MCRYPT_RIJNDAEL_128;
	// 			break;

	// 		case 192:
	// 			$this->cipher = MCRYPT_RIJNDAEL_192;
	// 			break;

	// 		case 256:
	// 			$this->cipher = MCRYPT_RIJNDAEL_256;
	// 			break;
	// 	}
	// }

	// /**
	// * 
	// * @param type $mode
	// */
	// public function setMode($mode) {
	// 	switch ($mode) {
	// 	case AESController::M_CBC:
	// 		$this->mode = MCRYPT_MODE_CBC;
	// 		Log::info('masuk cbc');
	// 		break;
	// 	case AESController::M_CFB:
	// 		$this->mode = MCRYPT_MODE_CFB;
	// 		break;
	// 	case AESController::M_ECB:
	// 		$this->mode = MCRYPT_MODE_ECB;
	// 		break;
	// 	case AESController::M_NOFB:
	// 		$this->mode = MCRYPT_MODE_NOFB;
	// 		break;
	// 	case AESController::M_OFB:
	// 		$this->mode = MCRYPT_MODE_OFB;
	// 		break;
	// 	case AESController::M_STREAM:
	// 		$this->mode = MCRYPT_MODE_STREAM;
	// 		break;
	// 	default:
	// 		$this->mode = MCRYPT_MODE_ECB;
	// 		break;
	// 	}
	// }

	// /**
	// * 
	// * @return boolean
	// */
	// public function validateParams() {
	// 	if ($this->data != null && $this->key != null && $this->cipher != null) {
	// 		return true;
	// 	} else {
	// 		return FALSE;
	// 	}
	// }

	// public function setIV($IV) {
	// 	$this->IV = $IV;
	// }
	// protected function getIV() {
	// 	if ($this->IV == "") {
	// 		$this->IV = mcrypt_create_iv(mcrypt_get_iv_size($this->cipher, $this->mode), MCRYPT_RAND);
	// 	}
	// 	return $this->IV;
	// }

	// /**
	// * @return type
	// * @throws Exception
	// */
	// public function encrypt() {

	// 	if ($this->validateParams()) {
	// 		return trim(base64_encode(mcrypt_encrypt($this->cipher, $this->key, $this->data, $this->mode, $this->getIV())));
	// 	} else {
	// 		throw new Exception('Invlid params!');
	// 	}
	// }
	// /**
	// * 
	// * @return type
	// * @throws Exception
	// */
	// public function decrypt() {
	// 	if ($this->validateParams()) {
	// 		return trim(mcrypt_decrypt($this->cipher, $this->key, base64_decode($this->data), $this->mode, $this->getIV()));
	// 	} else {
	// 		throw new Exception('Invlid params!');
	// 	}
	// }

	private $iv = '91vW253LnJV3ITdH'; #Same as in JAVA
	private $key = 'nEJwocCEwMO1tDPWSqObHajW0l9EWRuX'; #Same as in JAVA

	public function __construct()
	{
	}

	public function encrypt($str) 
	{

		//$key = $this->hex2bin($key);    
		$iv = $this->iv;

		$td = mcrypt_module_open('rijndael-128', '128', 'cbc', $iv);

		mcrypt_generic_init($td, $this->key, $iv);
		$encrypted = mcrypt_generic($td, $str);

		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);

		return bin2hex($encrypted);
	}

	public function decrypt($code) 
	{
		//$key = $this->hex2bin($key);
		$code = $this->hex2bin($code);
		$iv = $this->iv;

		$td = mcrypt_module_open('rijndael-128', '128', 'cbc', $iv);

		mcrypt_generic_init($td, $this->key, $iv);
		$decrypted = mdecrypt_generic($td, $code);

		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);

		return utf8_encode(trim($decrypted));
	}

	protected function hex2bin($hexdata) {
		$bindata = '';

		for ($i = 0; $i < strlen($hexdata); $i += 2) {
			$bindata .= chr(hexdec(substr($hexdata, $i, 2)));
		}

		return $bindata;
	}

        
}
