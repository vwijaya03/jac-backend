<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddCategoryRequest;
use App\Http\Requests\EditCategoryRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\CategoryModel;
use Auth, Log, JWTAuth;

class ApiCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function getCategory()
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$categories = CategoryModel::select('id', 'name')->where('delete', 0)->orderBy('name', 'asc')->get();

    	return response()->json(['result' => $categories]);
    }

    public function getSearchCategory(Request $request)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
        $category_name = $request->header('category_name');
        $result = [];

        Log::info('category name: '.$category_name);
        
        $categories = CategoryModel::select('id', 'name')
        ->where('name', 'like', '%'.$category_name.'%')
        ->where('delete', 0)
        ->orderBy('name', 'asc')
        ->paginate(20);

        if($categories == null)
        {
            $result = [];
        }
        else
        {
            $result = $categories;
        }

        return response()->json(['result' => $categories]);
    }

    public function postAddCategory(AddCategoryRequest $request)
    {	
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	CategoryModel::create([
    		'name' => $request->get('name'),
    		'delete' => 0,
    	]);

        return response()->json(['success' => 'Data kategori berhasil di tambahkan.']);
    }

    public function getEditCategory($id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$selected_category = CategoryModel::select('id', 'name')->where('id', $id)->where('delete', 0)->orderBy('name', 'asc')->first();

    	if($selected_category == null)
    	{
    		return response()->json(['error' => 'Data tidak tersedia.']);
    	}

    	return response()->json(['result' => $selected_category]);
    }

    public function postEditCategory(EditCategoryRequest $request, $id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$category = CategoryModel::select('id')
    	->where('id', $id)
    	->where('delete', 0)
    	->first();

    	if($category == null)
    	{
    		return response()->json(['error' => 'Data tidak tersedia.']);
    	}

    	CategoryModel::where('id', $id)->where('delete', 0)
    	->update([
    		'name' => $request->get('name'),
    	]);

    	return response()->json(['success' => 'Data kategori berhasil di update.']);
    }

    public function postDeleteCategory($id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	CategoryModel::where('id', $id)->where('delete', 0)
    	->update([
    		'delete' => 1
    	]);

    	return response()->json(['success' => 'Data kategori berhasil di hapus.']);
    }
}
