<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AESController;
use Illuminate\Http\Request;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\EditUserRequest;
use App\Http\Requests\ApproveUserRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\UserModel;
use Auth, Log, JWTAuth, Hash, Factory;

class ApiUserController extends Controller
{
    public function __construct()
    {
        // Note User, 0 Pending, 1 Approved, 2 Rejected
        $this->middleware('jwt.auth', ['except' => ['postAddUser', 'postResetPassword']]);
    }

    public function postChangePassword(Request $request)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }

        $current_password = $request->get('old_password');
        $new_password = $request->get('new_password');
        $email = $request->get('email');

        $aes = new AESController();

        $decrypted_current_password = $aes->decrypt($current_password);
        $decrypted_new_password = $aes->decrypt($new_password);
        

        if(Hash::check($decrypted_current_password, $current_user_auth->password))
        {
            Log::info('masuk if');
            UserModel::where('email', $email)->where('delete', 0)
            ->update([
                'password' => Hash::make($decrypted_new_password)
            ]);

            return response()->json(['success' => 'Password berhasil di ubah.']);
        }
        else
        {
            return response()->json(['error' => 'Password gagal di ubah.']);
        }
    }

    public function postResetPassword(Request $request)
    {
        $faker = \Faker\Factory::create();
        $reset_password = $faker->randomNumber($nbDigits = 6, $strict = false);
        $msg = '';
        $status_json = '';

        $email = $request->get('email');
        Log::info('new password: '.$reset_password);

        $result = UserModel::select('id', 'email')->where('email', $email)->where('delete', 0)->first();

        if($result != null)
        {
            UserModel::where('email', $email)->where('delete', 0)
            ->update([
                'password' => Hash::make($reset_password)
            ]);

            $sender = "jacautoparthella@gmail.com";

            $subject = "Reset Password Member JAC";

            $htmlContent = '
                <html>
                <head>
                    <title>Reset Password Member JAC</title>
                </head>
                <body>
                    <h1>Reset Password Member JAC</h1>
                    <table cellspacing="0" style="border: 2px dashed #FB4314; width: 600px; height: 200px;">
                        <tr>
                            <th>Password baru anda:</th><td>'.$reset_password.'</td>
                        </tr>
                    </table>
                </body>
                </html>';

            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            $headers .= 'From: JAC <'.$sender.'>' . "\r\n";
            $headers .= 'Reply-To: '.$sender.' ' . "\r\n";

            $check_mail = mail($result->email, 'Reset Password Member JAC', $htmlContent, $headers);

            if($check_mail){
                $status_json = 'success';
                $msg = 'Password berhasil di reset, silahkan cek email anda.';
            }
            else{
                $status_json = 'error';
                $msg = 'Gagal';
            }

            return response()->json([$status_json => $msg]);
        }
        else 
        {
            return response()->json(['error' => 'Data user tidak ditemukan.']);
        }
    }

    public function getUserPending()
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
        $users = UserModel::select('id', 'fullname', 'email', 'username', 'telepon', 'kota', 'kecamatan', 'kode_pos', 'tipe')->where('status', 0)->where('delete', 0)->orderBy('created_at', 'desc')->get();

        return response()->json(['result' => $users]);
    }

    public function getUserApproved()
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }

    	$users = UserModel::select('id', 'fullname', 'email', 'username', 'telepon', 'kota', 'kecamatan', 'kode_pos', 'tipe')->where('status', 1)->where('delete', 0)->orderBy('fullname', 'asc')->get();

    	return response()->json(['result' => $users]);
    }

    public function getUserRejected()
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }

        $users = UserModel::select('id', 'fullname', 'email', 'username', 'telepon', 'kota', 'kecamatan', 'kode_pos', 'tipe')->where('status', 2)->where('delete', 0)->get();

        return response()->json(['result' => $users]);
    }

    public function postAddUser(AddUserRequest $request)
    {	
        $aes = new AESController();
        $password = $request->get('password');
        $decrypted_password = $aes->decrypt($password);

        $existed_user = UserModel::select('id', 'username')->where('username', $request->get('username'))->where('delete', 0)->first();

        // $existed_email = UserModel::select('id', 'email')->where('email', $request->get('email'))->where('delete', 0)->first();

        if($existed_user != null)
        {
            return response()->json(['error' => 'Username telah digunakan !']);
        }

        // if($existed_email != null)
        // {
        //     return response()->json(['error' => 'Email telah digunakan !']);
        // }
        
    	UserModel::create([
            'fullname' => "not used",
            'email' => "not used",
            'username' => $request->get('username'),
            'password' => Hash::make($decrypted_password),
            'telepon' => "not used",
            'kota' => "not used",
            'kecamatan' => "not used",
            'kode_pos' => "not used",
    		'tipe' => "",
            'status' => 0,
    		'delete' => 0
    	]);

        return response()->json(['success' => 'Data anda sedang kami review.']);
    }

    public function getEditUser($id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }

    	$user = UserModel::select('id', 'fullname', 'email', 'username', 'telepon', 'kota', 'kecamatan', 'kode_pos', 'tipe')->where('id', $id)->where('status', 1)->where('delete', 0)->first();

    	if($user == null)
    	{
    		return response()->json(['error' => 'Data tidak tersedia.']);
    	}

    	return response()->json(['result' => $user]);
    }

    public function postEditUser(EditUserRequest $request, $id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }

        $aes = new AESController();
        $password = $request->get('password');
        $decrypted_password = $aes->decrypt($password);

    	$user = UserModel::select('id')
    	->where('id', $id)
        ->where('status', 1)
    	->where('delete', 0)
    	->first();

    	if($user == null)
    	{
    		return response()->json(['error' => 'Data tidak tersedia.']);
    	}

        if($decrypted_password != null)
        {
            UserModel::where('id', $id)->where('status', 1)->where('delete', 0)
            ->update([
                'fullname' => $request->get('fullname'),
                'email' => $request->get('email'),
                'username' => $request->get('username'),
                'password' => Hash::make($decrypted_password),
                'telepon' => $request->get('telepon'),
                'kota' => $request->get('kota'),
                'kecamatan' => $request->get('kecamatan'),
                'kode_pos' => $request->get('kode_pos'),
                'tipe' => $request->get('tipe')
            ]);
        }
        else
    	{
            UserModel::where('id', $id)->where('status', 1)->where('delete', 0)
            ->update([
                'fullname' => $request->get('fullname'),
                'email' => $request->get('email'),
                'username' => $request->get('username'),
                'telepon' => $request->get('telepon'),
                'kota' => $request->get('kota'),
                'kecamatan' => $request->get('kecamatan'),
                'kode_pos' => $request->get('kode_pos'),
                'tipe' => $request->get('tipe')
            ]);
        }

    	return response()->json(['success' => 'Data user berhasil di update.']);
    }

    public function postDeleteUser($id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }

    	UserModel::where('id', $id)->where('delete', 0)
    	->update([
    		'delete' => 1
    	]);

    	return response()->json(['success' => 'Data user berhasil di hapus.']);
    }

    public function postApproveUser(Request $request, $id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }

        $user = UserModel::select('id')
        ->where('id', $id)
        ->where('delete', 0)
        ->first();

        if($user == null)
        {
            return response()->json(['error' => 'Data tidak tersedia.']);
        }

        UserModel::where('id', $id)->where('delete', 0)
        ->update([
            'tipe' => $request->header('tipe'),
            'status' => 1
        ]);

        return response()->json(['success' => 'User telah di terima !']);
    }

    public function postRejectUser($id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }

        $user = UserModel::select('id')
        ->where('id', $id)
        ->where('delete', 0)
        ->first();

        if($user == null)
        {
            return response()->json(['error' => 'Data tidak tersedia.']);
        }

        UserModel::where('id', $id)->where('delete', 0)
        ->update([
            'status' => 2
        ]);

        return response()->json(['success' => 'User telah di tolak !']);
    }

    public function getSearchUserByName(Request $request)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }

        $name = $request->header('name');
        $status = $request->header('status');
        $result = [];

        Log::info($name);
        
        $users = UserModel::select('id', 'fullname', 'email', 'username', 'telepon', 'kota', 'kecamatan', 'kode_pos', 'tipe')
        ->where('fullname', 'like', '%'.$name.'%')
        ->where('status', $status)
        ->where('delete', 0)
        ->paginate(20);

        if($users == null)
        {
            $result = [];
        }
        else
        {
            $result = $users;
        }

        return response()->json(['result' => $result]);
    }
}
