<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ImageController;
use Illuminate\Http\Request;
use App\Http\Controllers\AESController;
use App\Http\Requests\AddProductRequest;
use App\Http\Requests\EditProductRequest;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\ProductModel;
use App\CategoryModel;
use Auth, Log;

class ApiProductController extends Controller
{
	private $mDir = '/public/product-image/';

    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['postLogin']]);
    }

    public function getProduct()
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$categories = CategoryModel::select('id', 'name')->where('delete', 0)->get();

    	$products = ProductModel::select('products.category_id', 'products.id as product_id', 'products.title', 'products.description', 'products.img_path1', 'categories.name as category_name')
    	->join('categories', 'categories.id', '=', 'products.category_id')
    	->where('products.delete', 0)
    	->where('categories.delete', 0)
        ->orderBy('products.created_at', 'desc')
    	->paginate(12);

    	return response()->json(['categories' => $categories, 'products' => $products]);
    }

    public function getAddProduct()
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$categories = CategoryModel::select('id', 'name')->where('delete', 0)->orderBy('name', 'asc')->get();

    	return response()->json(['result' => $categories]);
    }

    public function getSearchProductByTitleOrByCategory(Request $request)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }

    	$title = $request->header('title');
    	$category_id = $request->header('category_id');
    	$result = [];

        Log::info("isi title: ".$title);
        
        $categories = CategoryModel::select('id', 'name')->where('delete', 0)->orderBy('name', 'asc')->get();

        $products = ProductModel::select('products.id', 'products.category_id', 'products.title', 'products.description', 'products.img_path1', 'products.img_path2', 'products.img_path3', 'products.img_path4', 'categories.name as category_name')
        ->join('categories', 'categories.id', '=', 'products.category_id')
        ->where('products.title', 'like', '%'.$title.'%')
        ->where('products.delete', 0)
        ->where('categories.delete', 0)
        ->orderBy('products.created_at', 'desc')
        ->paginate(12);

        if($products == null)
        {
            $result = [];
        }
        else
        {
            $result = $products;
        }

    	if($category_id != null)
    	{
    		Log::info("isi category_id: ".$category_id);
    		$products = ProductModel::select('products.id', 'products.category_id', 'products.title', 'products.description', 'products.img_path1', 'products.img_path2', 'products.img_path3', 'products.img_path4', 'categories.name as category_name')
	    	->join('categories', 'categories.id', '=', 'products.category_id')
	    	->where('products.title', 'like', '%'.$title.'%')
	    	->where('products.category_id', $category_id)
	    	->where('products.delete', 0)
	    	->where('categories.delete', 0)
            ->orderBy('products.created_at', 'desc')
	    	->paginate(12);

	    	$result = $products;
    	}

    	return response()->json(['result' => $result, 'categories' => $categories]);
    }

    public function postAddProduct(AddProductRequest $request)
    {	
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$img_path1 = '';
    	$img_path2 = '';
    	$img_path3 = '';
    	$img_path4 = '';

    	if($request->hasFile('img_path1'))
    	{
    		$img_path1 = ImageController::createImage($request->file('img_path1'), $this->mDir, 'img_path1');
    	}

    	if($request->hasFile('img_path2'))
    	{
    		$img_path2 = ImageController::createImage($request->file('img_path2'), $this->mDir, 'img_path2');
    	}

    	if($request->hasFile('img_path3'))
    	{
    		$img_path3 = ImageController::createImage($request->file('img_path3'), $this->mDir, 'img_path3');
    	}

    	if($request->hasFile('img_path4'))
    	{
    		$img_path4 = ImageController::createImage($request->file('img_path4'), $this->mDir, 'img_path4');
    	}

    	ProductModel::create([
    		'category_id' => $request->get('category'),
    		'title' => $request->get('title'),
    		'description' => $request->get('description'),
    		'img_path1' => $img_path1,
    		'img_path2' => $img_path2,
    		'img_path3' => $img_path3,
    		'img_path4' => $img_path4,
    		'delete' => 0,
    	]);

        return response()->json(['success' => 'Data produk berhasil di tambahkan.']);
    }

    public function getEditProduct($id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$product = ProductModel::select('category_id', 'title', 'description', 'img_path1', 'img_path2', 'img_path3', 'img_path4')->where('id', $id)->where('delete', 0)->first();

    	if($product == null)
    	{
    		return response()->json(['error' => 'Data tidak tersedia.']);
    	}

    	// $selected_category = CategoryModel::select('id', 'name')->where('id', $product->category_id)->where('delete', 0)->first();

    	$categories = CategoryModel::select('id', 'name')->where('delete', 0)->orderBy('name', 'asc')->get();

    	return response()->json(['result' => $product, 'categories' => $categories]);
    }

    public function postEditProduct(EditProductRequest $request, $id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$product = ProductModel::select('img_path1', 'img_path2', 'img_path3', 'img_path4')
    	->where('id', $id)
    	->where('delete', 0)
    	->first();

    	if($product == null)
    	{
    		return response()->json(['error' => 'Data tidak tersedia.']);
    	}

    	$old_img_path1 = $product->img_path1;
    	$old_img_path2 = $product->img_path2;
    	$old_img_path3 = $product->img_path3;
    	$old_img_path4 = $product->img_path4;

    	if($request->hasFile('img_path1'))
    	{
    		ImageController::deleteImage($old_img_path1, 'img_path1');
    		$old_img_path1 = ImageController::createImage($request->file('img_path1'), $this->mDir, 'img_path1');
    	}

    	if($request->hasFile('img_path2'))
    	{
    		Log::info($old_img_path2);
    		ImageController::deleteImage($old_img_path2, 'img_path2');
    		$old_img_path2 = ImageController::createImage($request->file('img_path2'), $this->mDir, 'img_path2');
    	}

    	if($request->hasFile('img_path3'))
    	{
    		ImageController::deleteImage($old_img_path3, 'img_path3');
    		$old_img_path3 = ImageController::createImage($request->file('img_path3'), $this->mDir, 'img_path3');
    	}

    	if($request->hasFile('img_path4'))
    	{
    		ImageController::deleteImage($old_img_path4, 'img_path4');
    		$old_img_path4 = ImageController::createImage($request->file('img_path4'), $this->mDir, 'img_path4');
    	}

    	ProductModel::where('id', $id)->where('delete', 0)
    	->update([
    		'category_id' => $request->get('category'),
    		'title' => $request->get('title'),
    		'description' => $request->get('description'),
    		'img_path1' => $old_img_path1,
    		'img_path2' => $old_img_path2,
    		'img_path3' => $old_img_path3,
    		'img_path4' => $old_img_path4
    	]);

    	return response()->json(['success' => 'Data produk berhasil di update.']);
    }

    public function postDeleteProduct($id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	ProductModel::where('id', $id)->where('delete', 0)
    	->update([
    		'delete' => 1
    	]);

    	return response()->json(['success' => 'Data produk berhasil di hapus.']);
    }
}
