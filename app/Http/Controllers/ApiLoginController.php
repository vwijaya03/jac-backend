<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AESController;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Auth, Cache, Log;

// Key				Value
// Authorization	Bearer {isi_tokennya}

// Yang sekarang dipakai, ga pake Bearer
// Key              Value
// Authorization    {isi_tokennya}

class ApiLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['postLogin']]);
    }

    public function postLogin(LoginRequest $request)
    {
        $aes = new AESController();

        $username = $request->get('username');
        $password = $request->get('password');

        $decrypted_password = $aes->decrypt($password);

        Log::info("isi username: ".$username);
        // Log::info("isi decrypted password: ".$decrypted_password);
        Log::info("isi password: ".$password);
        
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt(['username' => $username, 'password' => $decrypted_password, 'status' => 1, 'delete' => 0])) {
                Auth::logout();
                return response()->json(['error' => 'invalid_credentials'], 200);
            }
        } catch (JWTException $e) {
            Auth::logout();
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = Auth::user();

        $token = JWTAuth::fromUser($user);

        $aes = new AESController();
        $encrypted_token = $aes->encrypt($token);

        return response()->json(['result' => $user, 'token' => $encrypted_token], 200);
    }

    public function postLogout()
    {
    	$existedToken = JWTAuth::getToken();
        if ($existedToken) {
            JWTAuth::invalidate($existedToken);
        }

        return response()->json(['success' => 'Logout Berhasil'], 200);
    }

    public static function isUserDeleted()
    {
        $user = JWTAuth::parseToken()->authenticate();

        if($user->delete == "1" || $user->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
    }
    
}



    // public function test(Request $request)
    // {
    //     $users = Auth::user();
    //     $aes = new AESController();
    //     $decrypted_token = $aes->decrypt($request->get('token'));
    //     $encrypted_data = $aes->encrypt(Auth::user());

    //     return response()->json(['users' => $encrypted_data, 'users_decrypt' => json_decode($decrypted_data)]);
    // }

    // public function getEncrypt(Request $request)
    // {
    //     $aes = new AESController();
    //     $encrypted_token = $aes->encrypt(Auth::user());

    //     return response()->json(['result' => Auth::user, 'encrypted_token' => $encrypted_token]);
    // }

    // public function getDecrypt(Request $request)
    // {
    //     $aes = new AESController();
    //     $decrypted_token = $aes->decrypt($request->header('Authorization'));

    //     return response()->json(['success' => 'well done.', 'decrypted_token' => $decrypted_token]);
    // }
// <?php

// namespace App\Http\Controllers;

// use App\Http\Controllers\AESController;
// use Illuminate\Http\Request;
// use App\Http\Requests\LoginRequest;
// use Auth;
// use JWTAuth;
// use Tymon\JWTAuth\Exceptions\JWTException;
// use Cache;

// // Key              Value
// // Authorization    Bearer {isi_tokennya}

// class ApiLoginController extends Controller
// {
//     public function __construct()
//     {
//         $this->middleware('jwt.auth', ['except' => ['postLogin']]);
//     }

//     public function getEncrypt()
//     {
//         $existedToken = JWTAuth::getToken();
//         $aes = new AESController();
//         $encrypted_token = $aes->encrypt($existedToken);

//         return response()->json(['result' => Auth::user(), 'encrypted_token' => $existedToken]);
//     }

//     public function getDecrypt()
//     {
//         $existedToken = JWTAuth::getToken();
//         $aes = new AESController();
//         $decrypted_token = $aes->decrypt($existedToken);

//         return response()->json(['success' => 'well done.', 'decrypted_token' => $decrypted_token]);
//     }

//     public function test()
//     {
//         $users = Auth::user();

//         return response()->json(['users' => $users]);
//     }

//     public function postLogin(LoginRequest $request)
//     {
        // $credentials = $request->only('username', 'password');

        // try {
        //     // attempt to verify the credentials and create a token for the user
        //     if (! $token = JWTAuth::attempt($credentials)) {
        //         return response()->json(['error' => 'invalid_credentials'], 401);
        //     }
        // } catch (JWTException $e) {
        //     // something went wrong whilst attempting to encode the token
        //     return response()->json(['error' => 'could_not_create_token'], 500);
        // }

        // $users = Auth::user();

        // $token = JWTAuth::getToken();

        // // $aes = new AESController();
        // // $encrypted_token = $aes->encrypt($token);

        // return response()->json(['result' => $users, 'token' => $token]);
//     }

//     public function getLogout()
//     {
//         $existedToken = JWTAuth::getToken();
//         if ($existedToken) {
//             JWTAuth::invalidate($existedToken);
//         }

//         return response()->json(['msg' => 'Logout Berhasil'], 200);
//     }
    
// }

