<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddPaymentRequest;
use App\Http\Requests\EditPaymentRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\PaymentModel;
use Auth, Log, JWTAuth;

class ApiPaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function getPayment()
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$payment = PaymentModel::select('id', 'description')
    	->where('delete', 0)
    	->orderBy('created_at', 'desc')
    	->first();

    	if($payment == null)
    	{
    		return response()->json(['result' => []]);
    	}

    	return response()->json(['result' => $payment]);
    }

    public function postAddPayment(AddPaymentRequest $request)
    {	
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$payment = PaymentModel::select('id', 'description')
    	->where('delete', 0)
    	->get();

    	if(count($payment) > 0)
    	{
    		return response()->json(['error' => 'Data promo sudah ada.']);
    	}

    	PaymentModel::create([
    		'description' => $request->get('description'),
    		'delete' => 0,
    	]);

        return response()->json(['success' => 'Data promo berhasil di tambahkan.']);
    }

    public function getEditPayment($id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$payment = PaymentModel::select('id', 'description')->where('id', $id)->where('delete', 0)->first();

    	if($payment == null)
    	{
    		return response()->json(['error' => 'Data tidak tersedia.']);
    	}

    	return response()->json(['result' => $payment]);
    }

    public function postEditPayment(EditPaymentRequest $request, $id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	$payment = PaymentModel::select('id')
    	->where('id', $id)
    	->where('delete', 0)
    	->first();

    	if($payment == null)
    	{
    		return response()->json(['error' => 'Data tidak tersedia.']);
    	}

    	PaymentModel::where('id', $id)->where('delete', 0)
    	->update([
    		'description' => $request->get('description'),
    	]);

    	return response()->json(['success' => 'Data promo berhasil di ubah.']);
    }

    public function postDeletePayment($id)
    {
        $current_user_auth = JWTAuth::parseToken()->authenticate();

        if($current_user_auth->delete == "1" || $current_user_auth->delete == 1)
        {   
            $current_user_token = JWTAuth::getToken();
            JWTAuth::invalidate($current_user_token);
            return response()->json(['error' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.'], 401);
        }
        
    	PaymentModel::where('id', $id)->where('delete', 0)
    	->update([
    		'delete' => 1
    	]);

    	return response()->json(['success' => 'Data promo berhasil di hapus.']);
    }
}
