<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }

    public function response(array $errors)
    {
        $err_array = [];
        
        if($errors != null)
        {
            foreach ($errors as $error) 
            {
                array_push($err_array, $error[0]);
            }
        }

        return response()->json(['error' => $err_array]);
    }
}
