<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'required',
            'title' => 'required',
            'description' => 'required',
            'img_path1' => '',
            'img_path2' => '',
            'img_path3' => '',
            'img_path4' => ''
        ];
    }

    public function response(array $errors)
    {
        $err_array = [];
        
        if($errors != null)
        {
            foreach ($errors as $error) 
            {
                array_push($err_array, $error[0]);
            }
        }

        return response()->json(['error' => $err_array]);
    }
}
