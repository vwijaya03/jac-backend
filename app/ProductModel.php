<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $fillable = ['category_id', 'title', 'description', 'img_path1', 'img_path2', 'img_path3', 'img_path4', 'delete'];
}
