<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'ApiLoginController@postLogin')->name('postLogin');
Route::post('/logout', 'ApiLoginController@postLogout')->name('postLogout');

// Payment
Route::get('/payment', 'ApiPaymentController@getPayment')->name('getPayment');

Route::post('/add-payment', 'ApiPaymentController@postAddPayment')->name('postAddPayment');

Route::get('/edit-payment/{id}', 'ApiPaymentController@getEditPayment')->name('getEditPayment');
Route::post('/edit-payment/{id}', 'ApiPaymentController@postEditPayment')->name('postEditPayment');

Route::post('/delete-payment/{id}', 'ApiPaymentController@postDeletePayment')->name('postDeletePayment');

// Retur
Route::get('/retur', 'ApiReturController@getRetur')->name('getRetur');

Route::post('/add-retur', 'ApiReturController@postAddRetur')->name('postAddRetur');

Route::get('/edit-retur/{id}', 'ApiReturController@getEditRetur')->name('getEditRetur');
Route::post('/edit-retur/{id}', 'ApiReturController@postEditRetur')->name('postEditRetur');

Route::post('/delete-retur/{id}', 'ApiReturController@postDeleteRetur')->name('postDeleteRetur');

// Category
Route::get('/category', 'ApiCategoryController@getCategory')->name('getCategory');

Route::get('/search-category', 'ApiCategoryController@getSearchCategory')->name('getSearchCategory');

// Route::get('/add-category', 'ApiCategoryController@getAddCategory')->name('getAddCategory');
Route::post('/add-category', 'ApiCategoryController@postAddCategory')->name('postAddCategory');

Route::get('/edit-category/{id}', 'ApiCategoryController@getEditCategory')->name('getEditCategory');
Route::post('/edit-category/{id}', 'ApiCategoryController@postEditCategory')->name('postEditCategory');

Route::post('/delete-category/{id}', 'ApiCategoryController@postDeleteCategory')->name('postDeleteCategory');

// Product
Route::get('/add-product', 'ApiProductController@getAddProduct')->name('getAddProduct');
Route::post('/add-product', 'ApiProductController@postAddProduct')->name('postAddProduct');

Route::get('/edit-product/{id}', 'ApiProductController@getEditProduct')->name('getEditProduct');
Route::post('/edit-product/{id}', 'ApiProductController@postEditProduct')->name('postEditProduct');

Route::post('/delete-product/{id}', 'ApiProductController@postDeleteProduct')->name('postDeleteProduct');

Route::get('/product', 'ApiProductController@getProduct')->name('getProduct');
Route::get('/search-product', 'ApiProductController@getSearchProductByTitleOrByCategory')->name('getSearchProductByTitleOrByCategory');

// User, 0 Pending, 1 Approved, 2 Rejected
Route::get('/user-approved', 'ApiUserController@getUserApproved')->name('getUserApproved');
Route::get('/user-pending', 'ApiUserController@getUserPending')->name('getUserPending');
Route::get('/user-rejected', 'ApiUserController@getUserRejected')->name('getUserRejected');

Route::get('/search-user', 'ApiUserController@getSearchUserByName')->name('getSearchUserByName');

Route::post('/add-user', 'ApiUserController@postAddUser')->name('postAddUser');

Route::get('/edit-user/{id}', 'ApiUserController@getEditUser')->name('getEditUser');
Route::post('/edit-user/{id}', 'ApiUserController@postEditUser')->name('postEditUser');

Route::post('/delete-user/{id}', 'ApiUserController@postDeleteUser')->name('postDeleteUser');

Route::post('/approve-user/{id}', 'ApiUserController@postApproveUser')->name('postApproveUser');
Route::post('/reject-user/{id}', 'ApiUserController@postRejectUser')->name('postRejectUser');

Route::post('/change-password', 'ApiUserController@postChangePassword')->name('postChangePassword');
Route::post('/reset-password', 'ApiUserController@postResetPassword')->name('postResetPassword');

// Route::get('/search-product-by-category/{category_id}/', 'ApiProductController@getProductByCategory')->name('getProductByCategory');
// Route::get('/search-product/{title}', 'ApiProductController@getProductByTitle')->name('getProductByTitle');